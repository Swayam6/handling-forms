import React from "react";
import isEmail from 'validator/lib/isEmail';
import Field from "./components/Field";


class App extends React.Component
{
  state = {
    fields: {
      name: '',
      email: ''
    },
    fieldErrors: {} ,
    people: [] 
  }; //Initial State

  onFormSubmit = (evt) => {
    evt.preventDefault();
    const person = this.state.fields;
    const people = [...this.state.people];
    
    if(this.validate()) return;

    this.setState({
      people: people.concat(person),
      fields: {
        name: '',
        email: ''
      }
    });
  }

  validate = () => {
    const person = this.state.fields;
    const fieldErrors = this.state.fieldErrors;

    const errorMessages = Object.keys(fieldErrors).filter(key => fieldErrors[key]);

    if(!person.name)return true;
    if(!person.email)return true;
    if(errorMessages.length) return true;

    return false;
  }

  onInputChange = ({name,value,error}) => {
    const fields = Object.assign({}, this.state.fields);
    const fieldErrors = Object.assign({},this.state.fieldErrors);


    fields[name] = value;
    fieldErrors[name] = error;

    this.setState({fields,fieldErrors});
  }

  render(){
    return (
      <div>
        <h1>Sign Up Sheet</h1>
        <form onSubmit={this.onFormSubmit}>
          <Field
            name="name"
            placeholder="Enter Name"
            value={this.state.fields.name}
            onChange={this.onInputChange}
            validate={val=> (val ? false : 'Name Required')}
          />
          <br/>
          <br/>
          <Field
            name="email"
            placeholder="Enter Email"
            value={this.state.fields.email}
            onChange={this.onInputChange}
            validate={val=> (isEmail(val) ? false : 'Invalid Email')}
          />
          <br/>
          <br/>
          <input type="submit"/>  
        </form>
        <div>
          <h3>Registered Names</h3>
          <ul>
            { this.state.people.map((people, i) => (
              <li key={i}>
                {people.name} - {people.email}
              </li>
            )) }
          </ul>
        </div>
      </div> 
    );
  }
}
//Why we should not use refs?
/**
 * By using refs we make the component uncontrolled because react doesn't knows the value that is being changed.
 * So the rendering of the element is not in control of react.
 * Whereas what we followed before this was that if input is changed we change it state so if state is changed then only it will render.
 * This is a controlled component.
 * 
 */
export default App;